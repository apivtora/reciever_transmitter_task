
#ifndef SEND_RECEIVE_H
#define SEND_RECEIVE_H

# include <errno.h>
# include <arpa/inet.h>
# include <string.h>
# include <stdlib.h>
# include <stdio.h>
# include <sys/un.h>
# include <sys/types.h>
# include <sys/socket.h>
# include <netinet/in.h>
# include <fcntl.h>
# include <unistd.h>
# include <pthread.h>

#define BUFF_SIZE 1024
#define MAX_FILENAME 255
#define RCV_IP "127.0.0.1"
#define RCV_PORT 3425

void receive_routine(char *ip_addr_char, char *name, int port_nbr);
void send_routine (char *ip_addr_char, char *filename, int port_nbr);
void usage_error(int exit_nbr);
void md5_check(char *filename);
void mc_receive(struct sockaddr_in *rcv_addr);
void *mc_send(void);


#endif