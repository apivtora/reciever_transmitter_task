
#include "send_receive.h"

#define MC_GROUP_IP "226.1.1.1"
#define IP_ADDR "127.0.0.1"
#define MC_PORT 4321
#define MAX_DATA_LENGTH 22

void *mc_send(void)
{
	struct in_addr local_interface;
	struct sockaddr_in addr;
	int sock;
	char data[MAX_DATA_LENGTH];
	sprintf(data, "%s %d", RCV_IP, RCV_PORT);
	sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock < 0)
	{
		printf("Eror with socket (multicast transm): %s\n", strerror(errno));
		exit(2);
	}
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr(MC_GROUP_IP);
	addr.sin_port = htons(MC_PORT);
	local_interface.s_addr = inet_addr(IP_ADDR);
	if(setsockopt(sock, IPPROTO_IP, IP_MULTICAST_IF, (char *)&local_interface, sizeof(local_interface)) < 0)
	{
		printf("Local Interface error(multicast transm): %s\n", strerror(errno));
  		exit(1);
	}
	while(1)
	{
		if (sendto(sock, data, sizeof(data), 0, (struct sockaddr*)&addr, sizeof(addr)) < 0)
			printf("Sending error (multicast transm): %s\n", strerror(errno));
		sleep(3);
	}
	/* non reacheble */
	return(0);
}