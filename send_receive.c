#include "send_receive.h"

#define PORT1 3425
#define PORT2 5000

#include <time.h>

void delay(int milliseconds)
{
    long pause;
    clock_t t1;
    clock_t t2;

    pause = milliseconds * (CLOCKS_PER_SEC/1000);
    t1 = clock();
    t2 = t1;
    while( (t1 - t2) < pause )
        t1 = clock();
}

void multicast_loop(void)
{
	pthread_t mc_thread;
	pthread_create(&mc_thread, NULL, &mc_send, NULL);
}

void usage_error(int exit_nbr)
{
	printf("Error occured, use as arguments\n[send] [IP addr] [file addr]\
for sending and \n[receive] [IP addr] for receiving\n");
	exit (exit_nbr);
}

int main(int argc, char **argv)
{
	char name[MAX_FILENAME];
	struct sockaddr_in rcv_addr;
	
	if (argc != 4 && (argc == !3 && !strcmp(argv[1], "receive")))
		usage_error(1);
	if (!strcmp(argv[1], "send"))
	{
		mc_receive(&rcv_addr);
		printf("PORT %d\n", rcv_addr.sin_port);
		send_routine(argv[2], argv[3], PORT1);
		receive_routine(argv[2], name, PORT2);
	}
	else if (!strcmp(argv[1], "receive"))
		{
			multicast_loop();
			while (1)
			{

				receive_routine(argv[2], name, PORT1);
				delay(100);
				send_routine(argv[2], name, PORT2);
			}
		}
	else
		usage_error(1);
}
