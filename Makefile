CC := gcc

CFLAGS := -Wall -lssl -lcrypto -lpthread
#  
OBJECTS := send_receive.o send_routine.o receive_routine.o md5_check.o mc_receive.o mc_send.o
HEADERS := send_receive.h

OUT := send_receive

all: $(OUT)

$(OUT) : $(OBJECTS) 
	$(CC) $^ $(CFLAGS) -o $@ 
	$ cp $(OUT) ../$(OUT)

clean:
	-rm -f $(OBJECTS)

fclean: clean
	-rm -f $(OUT)

re: fclean all


