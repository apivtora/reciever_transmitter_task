
#include "send_receive.h"

#define MC_GROUP_IP "226.1.1.1"
#define IP_ADDR "127.0.0.1"
#define MC_PORT 4321
#define MAX_DATA_LENGTH 22

static void copy_data(struct sockaddr_in *rcv_addr, char rcvd_data[MAX_DATA_LENGTH])
{
	char buff_ip[MAX_DATA_LENGTH];
	int i = 0;

	bzero(buff_ip, MAX_DATA_LENGTH);
	while (rcvd_data[i] != ' ')
	{
		buff_ip[i] = rcvd_data[i];
		i++;
	}
	rcv_addr->sin_addr.s_addr = inet_addr(buff_ip);
	rcv_addr->sin_port = atoi(rcvd_data + i);
}

void mc_receive(struct sockaddr_in *rcv_addr)
{
	struct sockaddr_in addr;
	int sock;
	struct ip_mreq mc_group;
	int reuse_opt = 1;
	char rcvd_data[MAX_DATA_LENGTH];

	sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock < 0)
	{
		printf("Eror with socket (multicast receiver): %s\n", strerror(errno));
		exit(2);
	}
	if(setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char *)&reuse_opt, sizeof(reuse_opt)) < 0)
	{
		printf("Reuse opt error(multicast receiver): %s\n", strerror(errno));
		exit(1);
	}
	addr.sin_family = AF_INET;
	addr.sin_port = htons(MC_PORT);
	addr.sin_addr.s_addr = INADDR_ANY;
	if(bind(sock, (struct sockaddr*)&addr, sizeof(addr)))
	{
		printf("Bind error (multicast receiver): %s\n", strerror(errno));	
		exit(1);
	}
	mc_group.imr_multiaddr.s_addr = inet_addr(MC_GROUP_IP);
	mc_group.imr_interface.s_addr = inet_addr(IP_ADDR);
	if(setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *)&mc_group, sizeof(mc_group)) < 0)
	{
		printf("Multicast group error (multicast receiver): %s\n", strerror(errno));	
		exit(1);
	}
	bzero(rcvd_data, sizeof(rcvd_data));
	if (recv(sock, rcvd_data, sizeof(rcvd_data), 0) < 0)
	{
		printf("Multicast(multicast receiver): %s\n", strerror(errno));	
		exit(1);
	}
	copy_data(rcv_addr, rcvd_data);
	printf("datagramm message: %s\n", rcvd_data);
	close(sock);
}