
#include "send_receive.h"
#include <openssl/md5.h>

void md5_check(char *filename)
{
	MD5_CTX mdContext;
	int bytes_read;
	char buff[BUFF_SIZE];
	int fd;
	unsigned char md5_buff[MD5_DIGEST_LENGTH];

	fd = open(filename, O_RDONLY);
	if (fd < 0)
	{
		printf("Failed to open file: %s\n", strerror(errno));
		exit(1);
	}
	MD5_Init (&mdContext);
	while ((bytes_read = read(fd, buff, BUFF_SIZE)) != 0)
        MD5_Update (&mdContext, buff, bytes_read);
    MD5_Final (md5_buff,&mdContext);
    bytes_read = 0;
    while (bytes_read < MD5_DIGEST_LENGTH)
    {
    	printf("%02x", md5_buff[bytes_read]);
    	bytes_read++;
    }
    printf("\n");
    close(fd);
}