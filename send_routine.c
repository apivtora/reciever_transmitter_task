#include "send_receive.h"

void send_routine (char *ip_addr_char, char *filename, int port_nbr)
{
	int sock;
	int fd;
	int bytes_read = 0;
	struct sockaddr_in addr;
	char buff[BUFF_SIZE];
	char name_buff[MAX_FILENAME];

	printf("Sending file with MD5 check sum ");
	md5_check(filename);

	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0)
	{
		printf("Eror with socket: %s\n", strerror(errno));
		exit(2);
	}
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port_nbr);
	addr.sin_addr.s_addr = inet_addr(ip_addr_char);
	if (connect(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0)
	{
		printf("Error on connect: %s\n", strerror(errno));
		usage_error(3);
	}

	fd = open(filename, O_RDONLY);
	if (fd < 0)
	{
		printf("Failed to open file: %s\n", strerror(errno));
		usage_error(4);
	}
	bzero(name_buff, MAX_FILENAME);
	while(filename[bytes_read])
	{
		name_buff[bytes_read] = filename[bytes_read];
		bytes_read++;
	}
	bytes_read = send(sock, name_buff, MAX_FILENAME, 0);
	bzero(buff, BUFF_SIZE);
	printf("sending bytes\n");
	while ((bytes_read = read(fd, buff, BUFF_SIZE)))
	{
		printf("%d bytes \n", bytes_read);
		send(sock, buff, bytes_read, 0);
		bzero(buff, BUFF_SIZE);
	}
	close (fd);
	close(sock);
}