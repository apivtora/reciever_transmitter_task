#include "send_receive.h"

void receive_routine(char *ip_addr_char, char *name, int port_nbr)
{
	int sock;
	int receiver;
	struct sockaddr_in addr;
	int fd;
	int bytes_read;
	char buff[BUFF_SIZE];
	// char name[MAX_FILENAME];


	receiver = socket(AF_INET, SOCK_STREAM, 0);
	if (receiver < 0)
	{
		printf("Eror with socket: %s\n", strerror(errno));
		exit(2);
	}
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port_nbr);
	addr.sin_addr.s_addr = inet_addr(ip_addr_char);
	// const int temp = 1;
	// setsockopt(receiver, SOL_SOCKET, SO_REUSEADDR, &temp, sizeof(temp));
	if(bind(receiver, (struct sockaddr *)&addr, sizeof(addr)) < 0)
	{
		printf("Bind error (server): %s\n", strerror(errno));
		exit(0);
	}
	listen(receiver,1);
	bzero(name, MAX_FILENAME);
	sock = accept(receiver, NULL, NULL);
	if (sock < 0)
	{
		printf("Eror with socket(server): %s\n", strerror(errno));
		exit (2);
	}
	recv(sock, name, MAX_FILENAME, 0);
		remove(name);
		fd = open(name, O_CREAT | O_RDWR, 0666);
		if (fd < 0)
		{	
			printf("Failed to open file (server): %s\n", strerror(errno));
			exit(4);
		}
	// bzero(buff, BUFF_SIZE);
	printf("receiving bytes\n");
	while ((bytes_read = recv(sock, buff, BUFF_SIZE, 0)))
	{
		printf("%d bytes\n", bytes_read);
		write(fd, buff, bytes_read);
		bzero(buff, BUFF_SIZE);
	}
	close(fd);
	printf("Recived file with MD5 check sum ");
	md5_check(name);
	close(sock);
	close(receiver);
}